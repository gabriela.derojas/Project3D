﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Accelerator : MonoBehaviour
{

    private Rigidbody2D rb;
    float dx;
    float speed = 5f;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    void Update()
    {
        transform.Translate(Input.acceleration.x, 0, 0);
    }

}
