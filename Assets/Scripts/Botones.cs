﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Botones : MonoBehaviour, IPointerUpHandler, IPointerDownHandler
{
    public bool leftpressed = false;
    public bool rightpressed = false;
    public float speed = 4.0f;
    public GameObject pj;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (leftpressed)
        {
            pj.transform.Translate(-(speed * Time.deltaTime), 0, 0);
        }

        if (rightpressed)
        {
            pj.transform.Translate(speed * Time.deltaTime, 0, 0);
        }
    }
    void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
    {
        leftpressed = false;
        rightpressed = false;
    }

    void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
    {
        if (this.gameObject.name == "izq")
        {
            leftpressed = true;
        }
        if (this.gameObject.name == "der")
        {
            rightpressed = true;
        }
    }
}
