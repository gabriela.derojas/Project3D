﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Brillo : MonoBehaviour
{
    public CharacterSelected character;
    public Slider slider;
    public float sliderValue;
    public Image image;
    void Start()
    {
        if (slider != null)
            slider.value = PlayerPrefs.GetFloat("brightness", 1.0f);
        Color tempColor = image.color;
        tempColor.a = (1 - PlayerPrefs.GetFloat("brightness", 1.0f)) * 0.75f;
        image.color = tempColor;
    }
    public void ChangeSlider(float valor)
    {
        sliderValue = valor;
        PlayerPrefs.SetFloat("brightness", sliderValue);
        Color tempColor = image.color;
        tempColor.a = (1 - PlayerPrefs.GetFloat("brightness", 1.0f)) * 0.75f;
        image.color = tempColor;
        character.brillo = tempColor;
    }
}
