﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeControll : MonoBehaviour
{
    public Button b1;
    public Button b2;
    public GameObject pj;
    public bool flag = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (flag)
        {
            b1.gameObject.GetComponent<Botones>().enabled = false;
            b2.gameObject.GetComponent<Botones>().enabled = false;
            pj.gameObject.GetComponent<Accelerator>().enabled = true;
        }
        else
        {
            b1.gameObject.GetComponent<Botones>().enabled = true;
            b2.gameObject.GetComponent<Botones>().enabled = true;
            pj.gameObject.GetComponent<Accelerator>().enabled = false;
        }
    }
    public void Change()
    {
       if(flag)
       {
            flag = false;
       }
       if(!flag)
        {
            flag = true;
        }
    }
}
