﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Character : MonoBehaviour
{
    public CharacterSelected charSelected;
    public GameObject[] characters;
    public UIInfo UI;
    public Text count_moneda;
    public int monedas = 0;
    void Start()
    {
        Instantiate(characters[charSelected.selected], this.transform);
    }
    void Update()
    {
        this.count_moneda.text = this.monedas.ToString();
    }
}
