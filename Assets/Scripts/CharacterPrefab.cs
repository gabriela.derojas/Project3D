﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Notifications.Android;

public class CharacterPrefab : MonoBehaviour
{
    private void Start()
    {
        AndroidNotificationChannel notificationChannel = new AndroidNotificationChannel()
        {
            Id = "channel id",
            Name = "Default Channel",
            Importance = Importance.Default,
            Description = "Notificacion generica"
        };
        AndroidNotificationCenter.RegisterNotificationChannel(notificationChannel);
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "moneda")
        {
            transform.parent.GetComponent<Character>().monedas++;
        }
        if (other.gameObject.tag == "caparazon")
        {
            transform.parent.GetComponent<Character>().UI.EndGame();
            AndroidNotification notification = new AndroidNotification()
            {
                Title = "Game Over",
                Text = "Intentalo de nuevo",
                SmallIcon = "default",
                LargeIcon = "default",
                FireTime = System.DateTime.Now.AddSeconds(5)
            };
            AndroidNotificationCenter.SendNotification(notification, "channel id");
        }
    }
}
