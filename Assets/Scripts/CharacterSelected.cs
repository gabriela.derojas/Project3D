﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class CharacterSelected : ScriptableObject
{
    public int selected;
    public Color brillo;
}
