﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSelector : MonoBehaviour
{
    public CharacterSelected charSelected;
    public void Selector(int i)
    {
        charSelected.selected = i;
    }
}
