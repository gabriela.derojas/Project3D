﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CuadrasMovement : MonoBehaviour
{
    public float speed;
    private void Start()
    {
        speed = 25;
    }
    void Update()
    {
        transform.Translate(new Vector3(speed, 0, 0) * Time.deltaTime);

        if (transform.position.x > 50) transform.position = new Vector3(-150, 0, 0);
    }
}
