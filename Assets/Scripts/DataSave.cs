﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Database;
using UnityEngine.UI;

public class DataSave : MonoBehaviour
{
    DatabaseReference reference;
    public Text kilomen;
    public Text timesurv;
    void Start()
    {
        reference = FirebaseDatabase.DefaultInstance.RootReference;
    }

public void SaveData()
    {
        Kilometros kil = new Kilometros();
        kil.kilom = kilomen.text;
        kil.surv = timesurv.text;
        string json = JsonUtility.ToJson(kil);
        reference.Child("Kilometros").Child(kil.kilom).SetRawJsonValueAsync(json).ContinueWith(
            task =>
            {
                if(task.IsCompleted)
                {
                    print("Se almaceno correctamente");
                }
                else
                {
                    print("No se almaceno correctamente");
                }
            });
        
    }
}
