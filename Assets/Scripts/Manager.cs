﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class Manager : MonoBehaviour
{
    public CharacterSelected character;
    public Image brillo;
    public Button StartButton;
    public string SceneName;

    // Start is called before the first frame update
    void Start()
    {
        character.selected = 0;
        if (character.brillo != null) brillo.color = character.brillo;
        else character.brillo = brillo.color;
        Time.timeScale = 1f;
        StartButton.onClick.AddListener(() => ActivateLoadingCanvas());
    }
    void LoadScene()
    {
        SceneManager.LoadScene(SceneName);
    }
    public void ActivateLoadingCanvas()
    {
        StartCoroutine(LoadLevelWithRealProcess());
    }
    IEnumerator LoadLevelWithRealProcess()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        AsyncOperation async = SceneManager.LoadSceneAsync(SceneName);
        while(!async.isDone)
        {
            yield return null;
        }
    }
}
