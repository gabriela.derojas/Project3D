﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ObjectMove : MonoBehaviour
{
    public float speed;
    public pool pool_manager;
    public float spawnZ = 0;
    public float spawnZMax = 5.0f;
    public float spawnZMin = -5.0f;
    public GameObject SpawnPoint;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.Translate(Vector3.forward * speed * Time.deltaTime);
        if(this.gameObject.tag == "platano")
        {
            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, (this.transform.position.z + Mathf.Sin(Time.time))/1.3f);
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Collider")
        {
            pool_manager.SetObject(this.gameObject);
        }
        if (other.gameObject.tag == "caparazon" && other.gameObject.tag == "platano")
        {
            //codigo para morir
            
        }

    }
    public void Initialice(pool manager, GameObject spawn)
    {
        pool_manager = manager;
        this.SpawnPoint = spawn;
    }
    public void SetPosition()
    {
        this.spawnZ = Random.Range(spawnZMin, spawnZMax);
        Vector3 pos = new Vector3(this.SpawnPoint.transform.position.x, this.SpawnPoint.transform.position.y, this.spawnZ);
    }
}
