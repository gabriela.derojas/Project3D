﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SelectorEventIcon : MonoBehaviour, IPointerDownHandler
{
    public int id;
    public CharacterSelected selected;
    public Image selectedBG;
    public Image notSelectedBG1;
    public Image notSelectedBG2;
    void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
    {
        selected.selected = id;
        selectedBG.color = Color.green;
        notSelectedBG1.color = Color.yellow;
        notSelectedBG2.color = Color.yellow;
    }
}
