﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIInfo : MonoBehaviour
{
    public CharacterSelected character;
    public Image brillo;
    public Text Distance;
    public Text Tiempo;
    public Image bar;
    float km;
    float time;
    float fill;
    public List<CuadrasMovement> cuadras;
    public Button turboButton;

    public Text pauseText;
    public GameObject pausePanel;
    public GameObject resumeButton;
    public DataSave info;

    bool turboFilling;
    void Start()
    {
        brillo.color = character.brillo;
        turboFilling = true;
        Distance.text = "Distance: 0 km";
    }

    void Update()
    {
        km += Time.deltaTime * 1.5f * cuadras[0].speed / 25;
        time += Time.deltaTime;
        if (turboFilling) fill += 0.05f * Time.deltaTime;
        bar.fillAmount = Mathf.Clamp01(fill);
        if (fill >= 1) turboButton.gameObject.SetActive(true);
        Distance.text = "Distance: " + Mathf.RoundToInt(km) + " km";
        Tiempo.text = (time).ToString("F1") + "s";    
    }

    public void TurboButton()
    {
        fill = 0;
        bar.fillAmount = 0;
        turboFilling = false;
        foreach (CuadrasMovement cuadra in cuadras)
        {
            cuadra.speed = 50;
        }
        Wait();
        turboButton.gameObject.SetActive(false);
    }
    public async void Wait()
    {
        await System.Threading.Tasks.Task.Delay(System.TimeSpan.FromSeconds(3));
        turboFilling = true;
        foreach (CuadrasMovement cuadra in cuadras)
        {
            cuadra.speed = 25;
        }
    }
    public void Menu()
    {
        SceneManager.LoadScene("Menu");
    }
    public void Pause()
    {
        pausePanel.SetActive(true);
        resumeButton.SetActive(true);
        pauseText.text = "Pause";
        Time.timeScale = 0f;
    }
    public void Resume()
    {
        Time.timeScale = 1f;
    }
    public void EndGame()
    {
        pausePanel.SetActive(true);
        resumeButton.SetActive(false);
        pauseText.text = "Game Over";
        Time.timeScale = 0f;
        info.SaveData();
    }
    public void Quit()
    {
        Application.Quit();
    }
}
