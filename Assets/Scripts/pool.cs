﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pool : MonoBehaviour
{
    public List<GameObject> Pool;
    public GameObject caparazon;
    public int max;
    public float spawnRateMax = 1.0f;
    public float spawnRateMin = 7.0f;
    public float spawnRate;
    public GameObject SpawnPoint;
    public bool waveIsDone = true;
    public float spawnZ = 0;
    public float spawnZMax = 5.0f;
    public float spawnZMin = -5.0f;
    // Start is called before the first frame update
    void Start()
    {
        InstansiateObjects();
    }

    // Update is called once per frame
    void Update()
    {
        if (waveIsDone == true)
        {
            StartCoroutine(waveSpawner());
        }
    }
    public void InstansiateObjects()
    {
        GameObject temp;
        for(int i = 0; i < this.max; i++)
        {
            this.spawnZ = Random.Range(spawnZMin, spawnZMax);
            Vector3 pos = new Vector3(this.SpawnPoint.transform.position.x, this.SpawnPoint.transform.position.y, this.spawnZ);
            temp = Instantiate(caparazon, pos, this.SpawnPoint.transform.rotation);
            Pool.Add(temp);
            temp.SetActive(false);
            temp.transform.SetParent(this.transform);
            temp.GetComponent<ObjectMove>().Initialice(this,SpawnPoint);

        }
    }
    public void GetObject()
    {
        if(Pool.Count > 0)
        {
            GameObject temp = Pool[0];
            Pool.RemoveAt(0);
            temp.SetActive(true);
            temp.GetComponent<ObjectMove>().SetPosition();
        }
    }
    public void SetObject(GameObject objeto)
    {
        
        if (Pool.Count > 0)
        {
            GameObject temp = Pool[0];
            Pool.Add(temp);
            objeto.GetComponent<ObjectMove>().SetPosition();
            objeto.SetActive(false);
        }
    }
    IEnumerator waveSpawner()
    {
        waveIsDone = false;
        GetObject();
        this.spawnRate = Random.Range(this.spawnRateMin, this.spawnRateMax);
        yield return new WaitForSeconds(spawnRate);

        waveIsDone = true;
    }
}
